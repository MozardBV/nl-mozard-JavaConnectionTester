<!-- PROJECT SHIELDS -->

<!-- PROJECT LOGO -->
<!-- markdownlint-disable MD033 -->
<br />
<p align="center" style="text-align: center;">
  <h3 style="text-align: center;" align="center">mozardbv/nl-mozard-JavaConnectionTester</h3>
  <p style="text-align: center;" align="center">
  <span>Verbinding testen met Mozard SaaS</span>
    <a href="https://mozardbv.gitlab.io/nl-mozard-JavaConnectionTester">
      <strong>Bekijk de docs »</strong>
    </a>
    <br /><br />
    <a href="https://intranet.mozard.nl/mozard/!suite09.scherm1089?mWfr=367">Meld een bevinding</a>
    <a href="https://intranet.mozard.nl/mozard/!suite09.scherm1089?mWfr=604&mDdv=990842">Verzoek nieuwe functionaliteit</a>
  </p>
</p>
<!-- markdownlint-enable MD033 -->

<!-- INHOUDSOPGAVE -->

<!-- markdownlint-disable MD041 -->

## Inhoudsopgave

- [Over dit project](#over-dit-project)
- [Aan de slag](#aan-de-slag)
  - [Afhankelijkheden](#afhankelijkheden)
  - [Installatie](#installatie)
- [Gebruik](#gebruik)
- [Gebouwd met](#gebouwd-met)
- [Versioning](#versioning)
- [Auteurs](#auteurs)
- [Licentie](#licentie)

## Over dit project

## Aan de slag

### Afhankelijkheden

- `java` >= 8.0.0

### Installatie

Download `bin/app.jar` of clone de gehele repository:

`$ git clone https://gitlab.comMozardBV/nl-mozard-JavaConnectionTester.git`

## Gebruik

`$ java -jar app.jar`

## Gebouwd met

- [Java](https://www.java.com/en/)

## Versioning

Niet van toepassing.

## Auteurs

- **Patrick Godschalk (Mozard)** - _Ontwikkelaar_ - [pgodschalk](https://gitlab.com/pgodschalk)

Zie ook de lijst van [contributors](https://gitlab.com/mozardbv/nl-mozard-javaconnectiontester/-/graphs/master) die hebben bijgedragen aan dit project.

## Licentie

[SPDX](https://spdx.org/licenses/) license: `UNLICENSED`

Copyright (c) 2006-2020 Mozard B.V. - Alle rechten voorbehouden.

[Leveringsvoorwaarden](https://www.mozard.nl/mozard/!suite86.scherm0325?mPag=204&mLok=1)
