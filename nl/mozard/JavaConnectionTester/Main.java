package nl.mozard.JavaConnectionTester;

import java.net.*;
import java.io.*;
import java.util.List;
import java.util.Map;


/**
 * Kijken of een verbinding mogelijk is met Mozard Saas, gebruikt alleen
 * standaard Java libs.
 *
 * @param connTo Een URL om mee te verbinden
 */
public class Main {
  private static void testConnection(URL connTo) throws Exception {
    // Verbinding opzetten
    URLConnection conn = connTo.openConnection();
    BufferedReader in = new BufferedReader (
      new InputStreamReader(
        conn.getInputStream()
      )
    );

    // Var voor een buffer
    String inputLine;

    System.out.println("\n\n\n*** Resultaat verbinding met " + connTo + " ***\n");

    // Response headers printen
    System.out.println("== Headers ==");
    Map<String, List<String>> map = conn.getHeaderFields();

    for (Map.Entry<String, List<String>> entry : map.entrySet()) {
      System.out.println(entry.getKey() + ": " + entry.getValue());
    }

    // Body printen
    System.out.println("\n\n== Body ==");

    while ((inputLine = in.readLine()) != null) {
      System.out.println(inputLine);
    }

    // Verbinding weer sluiten
    in.close();
  }

  public static void main (String[] args) throws Exception {
    // Zou altijd 200 (ok) terug moeten geven (tenzij omgeving down)
    // Inhoud van HTML zijn omgevingsparameters
    URL mozard  = new URL ("https://duawp.mozard.nl/mozard/!suite86.test?t=cgi");
    // Zou altijd 302 (redirect) terug moeten geven naar /nextBASE/authenticate
    // Inhoud van HTML is een redirectmelding
    URL xential = new URL ("https://duawp-xl.mozard.nl/nextDocuments/manager");

    testConnection(mozard);
    testConnection(xential);
  }
}
